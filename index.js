// https://jsonplaceholder.typicode.com/todos 


///////////////
fetch("https://jsonplaceholder.typicode.com/todos", 
	{
		method: "GET",
		headers: {
			"Content-Type": "application/json"
		}
	}
) 
.then((response) => response.json())
.then((response) => console.log(response));

//Map
fetch("https://jsonplaceholder.typicode.com/todos")
.then((response) => response.json())
.then((json) => {

	let list = json.map((todo => {
		return todo.title;
	}))
	console.log(list);
});
 // OR
// .then(json => {console.log(json.map(todo => (todo.title)))})





// GET
fetch("https://jsonplaceholder.typicode.com/todos/1", 
	{
		method: "GET",
		headers: {
			"Content-Type": "application/json"
		}
	}
)
.then((response) => response.json())
.then((response) => console.log(response));


// Getting specific item

fetch("https://jsonplaceholder.typicode.com/todos/1")
.then((response) => response.json())
.then((json) => console.log(`The item "${json.title}" on the list has a status of ${json.completed}`))

// POST
fetch("https://jsonplaceholder.typicode.com/todos", 
	{
		method: "POST",
		headers: {
			"Content-Type": "application/json"
		},
		body: JSON.stringify({
			completed: false,
			title: "Created To Do List Item",
			userId: 1
		})
	}
)
.then((response) => response.json())
.then((response) => console.log(response))


// PUT
fetch("https://jsonplaceholder.typicode.com/todos/1", 
	{
		method: "PUT",
		headers: {
			"Content-Type": "application/json"
		},
		body: JSON.stringify({
			dateCompleted: "Pending",
			description: "To update the to do list with a different data structure",
			status: "Pending",
			title: "Updated To Do List Item",
			userId: 1
		})
	}
)
.then((response) => response.json())
.then((response) => console.log(response))


// PATCH
fetch("https://jsonplaceholder.typicode.com/todos/1", 
	{
		method: "PATCH",
		headers: {
			"Content-Type": "application/json"
		},
		body: JSON.stringify({
			dateCompleted: "07/09/21",
			status: "Complete"
		})
	}
)
.then((response) => response.json())
.then((response) => console.log(response))

fetch("https://jsonplaceholder.typicode.com/todos/1",
	{
		method: "DELETE"
	}
)
.then((response) => response.json())
.then((response) => console.log(response));